package morpion.jeu;

import java.util.TimerTask;

/**
 *
 * @author root
 */
class MyTask extends TimerTask 
{

    private int times;

    @Override
    public void run()
    {
        times--;
        if (times > 0)
	{
            System.out.print(times + " ");
        }
    }
    
    public void setTime (int x)
    {
	times = x;

    }
    
    public int getTime()
    {
	return(times);
    }
}
