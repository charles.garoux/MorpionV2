package morpion.jeu;

/**
 *
 * @author Charles Garoux
 */
public class Player
{
    private String name = new String();
    
    private char piece;
    
    public Player()
    {
	this.setNameAndPiece("Unknown", '?');
    }
    
    public Player(String name, char piece)
    {
	this.setNameAndPiece(name, piece);
    }
    
    //accessors
    public String getName()
    {
	return this.name;
    }
    
    public char getPiece()
    {
	return this.piece;
    }
    //mutators
    public void setName(String name)
    {
	this.name = name;
    }
    
    public void setPiece(char piece)
    {
	this.piece = piece;
    }
    
    public void setNameAndPiece(String name, char piece)
    {
	this.setName(name);
	this.setPiece(piece);
    }
}