package morpion.jeu;

import java.util.Scanner;
import java.util.Timer;

/**
 *
 * @author Charles Garoux
 */
public class Morpion {
   
    public static void main(String[] args)
    {
	int turn;
	char piece;
	String name;
	char choice;
	int timePerRound;
	Scanner Keyboard = new Scanner(System.in);
	Player Player1 = new Player();
	Player Player2 = new Player();
	
	Timer timer = new Timer("Printer");         //Instancier le chrono
        MyTask t = new MyTask();                    //Instanicer l'objet de réglage du chrono
	System.out.println("How long for round (in seconds):");
	timePerRound = Keyboard.nextInt();
	
	System.out.println("Default players or custome  players?[input any key for default player or 'c' for custom player]");
	choice = inputChar();
	if (choice == 'c')
	{
	    System.out.println("Input the name of Player1:");
	    name = Keyboard.next();
	    System.out.println("Now input the piece you want:");
	    piece = inputChar();
	    Player1.setNameAndPiece(name, piece);
	    System.out.println(Player1.getName());
	    
	    System.out.println("Input the name of Player2:");
	    name = Keyboard.next();
	    System.out.println("Now input the piece you want:");
	    piece = inputChar();
	    Player2.setNameAndPiece(name, piece);
	}
	else
	{
	    Player1.setNameAndPiece("Player One", 'O');
	    Player2.setNameAndPiece("Player Two", 'X');	
	}
	BoardGame Board = new BoardGame();
	System.out.println("The game start, good luck!");
	Board.printBoardGame();
	timer.schedule(t, 0, 2000);
	turn = 0;
	while(true)
	{
	    t.setTime(timePerRound);
	    System.out.println("It's your turn " + Player1.getName() + " chose a case to put your piece:");
	    choice = Board.choiceCase(inputChar());
	    if(t.getTime() > 0)
	    {
		Board.putThePiece(Player1.getPiece(), choice);
		turn++;
	    }
	    else
		System.out.println("Too late, next player!");
	    Board.printBoardGame();
	    if(checkForVictory(Board, Player1, Player2) == 1)
		System.exit(0);
	    if (turn == 9)
	    {
		System.out.println("Equality!");
		System.exit(0);
	    }
	    t.setTime(timePerRound);
	    System.out.println("It's your turn " + Player2.getName() + " chose a case to put your piece:");
	    choice = Board.choiceCase(inputChar());
	    if(t.getTime() > 0)
	    {
		Board.putThePiece(Player1.getPiece(), choice);
		turn++;
	    }
	    else
		System.out.println("Too late, next player!");
	    Board.printBoardGame();
	    if(checkForVictory(Board, Player1, Player2) == 1)
		System.exit(0);
	}
    }
    
    private static int checkForVictory(BoardGame Board, Player Player1, Player Player2)
    {
	if(Board.whichPieceWins() == Player1.getPiece())
	{
	    System.out.println(Player1.getName() + " win the game!");
	    return 1;
	}
	else if (Board.whichPieceWins() == Player2.getPiece())
	{
	    System.out.println(Player2.getName() + " win the game!");
	    return 1;
	}
	return 0;
    }
    
    public static char inputChar()
    {
	char c;
	Scanner Key = new Scanner(System.in);
	c = Key.next().charAt(0);	    //take juste the first char of the string
	return c;
    }
}
