package morpion.jeu;

import java.util.Scanner;

/**
 *
 * @author Charles Garoux
 */

public class BoardGame
{
    private char boardGame[][];
    //constructor
    public BoardGame()
    {
	this.boardGame = new char[][]{
				{'1', '2', '3'},
				{'4', '5', '6'},
				{'7', '8', '9'}};
    }
    //accessor
    public char getBoardGame(int x, int y)
    {
       char c;
	       
       c = this.boardGame[y][x];
       return c;
    }
   //mutator
    public void setBoardGame(int x, int y, char c)
    {
       this.boardGame[y][x] = c;
    }
    
    public char getSquare(char num)
    {
	char c;
	
	if(num == '1')
	    c = this.getBoardGame(0,0);
	else if (num == '2')
	    c = this.getBoardGame(1,0);
	else if (num == '3')
	    c = this.getBoardGame(2,0);
	else if (num == '4')
	    c = this.getBoardGame(0,1);
	else if (num == '5')
	    c = this.getBoardGame(1,1);
	else if (num == '6')
	    c = this.getBoardGame(2,1);
	else if (num == '7')
	    c = this.getBoardGame(0,2);
	else if (num == '8')
	    c = this.getBoardGame(1,2);
	else if (num == '9')
	    c = this.getBoardGame(2,2);
	else
	{
	    System.out.println("Error (getSquare) in parameter : " + num);
	    return ' ';
	}   
	return c;
    }
    
    public void setSquare(char num, char c)
    {
	if(num == '1')
	    this.setBoardGame(0, 0, c);
	else if (num == '2')
	    this.setBoardGame(1, 0, c);
	else if (num == '3')
	    this.setBoardGame(2, 0, c);
	else if (num == '4')
	    this.setBoardGame(0, 1, c);
	else if (num == '5')
	    this.setBoardGame(1, 1, c);
	else if (num == '6')
	    this.setBoardGame(2, 1, c);
	else if (num == '7')
	    this.setBoardGame(0, 2, c);
	else if (num == '8')
	    this.setBoardGame(1, 2, c);
	else if (num == '9')
	    this.setBoardGame(2, 2, c);
	else
	{
	    System.out.println("Error (setSquare) in parameter : " + num + "and" + c);
	    System.exit(1);
	}
    }
    
    private int caseIsFree(char num)
    {
	if (this.getSquare(num) >= '1' && this.getSquare(num) <= '9')
	    return 1;
	return 0;
    }
    
    public char choiceCase(char num)
    {
	Scanner Key = new Scanner(System.in);
	
	while(this.caseIsFree(num) == 0)
	{
	    System.out.println("Input a free case:");
	    num = Key.next().charAt(0);
	}
	return(num);
    }
    
    public int putThePiece(char piece, char num)
    {
	Scanner Key = new Scanner(System.in);
	
	while(this.caseIsFree(num) == 0)
	{
	    System.out.println("Input a free case:");
	    num = Key.next().charAt(0);
	}
	this.setSquare(num, piece);
	return(1);
    }

    /*
	Printing of the board game
    */
    private void printLine()
    {
	System.out.println("+-+-+-+");
    }
    
    private void printLine(char a, char b, char c)
    {
	System.out.println("|" + this.getSquare(a) + "|" + this.getSquare(b) + "|" + this.getSquare(c)+ "|");
    }
    
    public void printBoardGame()
    {
	this.printLine();
	this.printLine('1', '2', '3');
	this.printLine();
	this.printLine('4', '5', '6');
	this.printLine();
	this.printLine('7', '8', '9');
	this.printLine();
    }
    
/*
    Check for victory :
*/    
    private char checkLine(int x)
    {
	if(this.getBoardGame(0, x) == this.getBoardGame(1, x) && this.getBoardGame(1, x) == this.getBoardGame(2, x))
	    return this.getBoardGame(0, x);	//victory, return the symbole who win
	return '0';
    }
    
    private char checkColumn(int y)
    {
	if(this.getBoardGame(y, 0) == this.getBoardGame(y, 1) && this.getBoardGame(y, 1) == this.getBoardGame(y, 2))
	    return this.getBoardGame(y, 0);	//victory, return the symbole who win
	return '0';
    }
    
    private char checkDiagonal1()   //Diagonal 1 is \
    {
	if(this.getBoardGame(0, 0) == this.getBoardGame(1, 1) && this.getBoardGame(1, 1) == this.getBoardGame(2, 2))
	    return this.getBoardGame(0, 0);	//victory, return the symbole who win
	return '0';
    }
    
    private char checkDiagonal2()   //Diagonal 2 is /
    {
	if(this.getBoardGame(0, 2) == this.getBoardGame(1, 1) && this.getBoardGame(1, 1) == this.getBoardGame(2, 0))
	    return this.getBoardGame(0, 0);	//victory, return the symbole who win
	return '0';
    }
    
    public char whichPieceWins()
    {
	int i;
	
	i = 0;
	while(i < 3)
	{
	    if (this.checkLine(i) != '0')
		return this.checkLine(i);
	    else if (this.checkColumn(i) != '0')
		return this.checkColumn(i);
	    i++;
	}
	if (this.checkDiagonal1() != '0')
	    return this.checkDiagonal1();
	else if (this.checkDiagonal2() != '0')
	    return this.checkDiagonal2();
	return '0';
    }
    
    public void resetBoard()
    {
	this.boardGame = new char[][]{
				{'1', '2', '3'},
				{'4', '5', '6'},
				{'7', '8', '9'}};
    }
}
